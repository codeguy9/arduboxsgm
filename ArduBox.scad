/*

File ArduBox.scad

Scott Martin
13-MAR-2015

*/

//use <Write.scad>;
//use <ujoint.scad>;
//use <GearScriptThing3575.scad>;

//use <..\Scott_Parts001.scad>;
//use <..\Scott_Parts002.scad>;
//use <..\Scott_Parts003.scad>;
//use <Spiff.scad>;


// C:/Scott/CurrentWorkOpenscad/RightAngleAssembly/RightAngleAssembly.scad
// C:/Scott/home_backup/OpenSCAD/openscad-2014.03/Work/ArduBox/ArduBox.scad

/*


Notes:

Use 3mm rod, nuts and washers through the corners to hold box sections together.

Use 10/24 bolts to mount the feet if needed.

The spacers can be used if you dont use the feet to mount the box with.

The arduino mount holes are about 3mm.

You can mount any number of bottom, middle sections, plates etc together in
any configuration you desire.

If you need to add switches or anything else you can just drill holes where needed.

*/


module DoIt(){

   height_of_hexagons = 25; //will be used to punch holes
   row_spacing = (radius_of_hexagons * 2.3);
   row_spacing_adj = 1.125;
   m3_rad = 1.75;
   Bolt_hole_od_10_24_inch_fit = 6;


   module makeHexagon(radius_of_hexagons){
      cylinder(r = radius_of_hexagons, h = height_of_hexagons, center = True, $fn=6);
   }


   module makeRow2(hex_count,radius_of_hexagons){

	   row_spacing2 = (radius_of_hexagons * 3.5);
      row_spacing_adj2 = radius_of_hexagons / 7.23;

      for ( i = [1 : 1 : hex_count] )
      {
         translate([(i * row_spacing2) * row_spacing_adj, 0, 0])
         makeHexagon(radius_of_hexagons);
      }

   }


   // different style
   module makeHexGrid2(cols, rows, radius_of_hexagons){

      row_spacing2 = (radius_of_hexagons * 3.5);
      row_spacing_adj2 = radius_of_hexagons / 7.23;
      y_adj = 3;
    
      module one(){
         makeRow2(cols,radius_of_hexagons);
      }
   
      module two(){
         translate([ (row_spacing2 / 2) + row_spacing_adj2*1.8, -(row_spacing2/y_adj), 0 ])
         makeRow2(cols-1,radius_of_hexagons);
      }
   
      module three(){
         translate([ 0, -(( row_spacing2 / y_adj) * (3 - 1) ), 0 ])
         makeRow2(cols,radius_of_hexagons);
      }

      module four(){
         translate([ (row_spacing2 / 2) + row_spacing_adj2*1.8, -( row_spacing2/y_adj * (4 - 1) ) , 0 ])
         makeRow2(cols-1,radius_of_hexagons);   
      }

      module five(){
         translate([ 0, -(( row_spacing2 / y_adj) * (5 - 1) ), 0 ])
         makeRow2(cols,radius_of_hexagons); 
      }

      module six(){
         translate([ (row_spacing2 / 2) + row_spacing_adj2*1.8, -( row_spacing2/y_adj * (6 - 1) ) , 0 ])
         makeRow2(cols-1,radius_of_hexagons);   
      }

      module seven(){
         translate([ 0, -(( row_spacing2 / y_adj) * (7 - 1) ), 0 ])
         makeRow2(cols,radius_of_hexagons); 
      }

      module eight(){
         translate([ (row_spacing2 / 2) + row_spacing_adj2*1.8, -( row_spacing2/y_adj * (8 - 1) ) , 0 ])
         makeRow2(cols-1,radius_of_hexagons);   
      }

      module nine(){
         translate([ 0, -(( row_spacing2 / y_adj) * (9 - 1) ), 0 ])
         makeRow2(cols,radius_of_hexagons); 
      }

      if(rows==1){ one(); }
      if(rows==2){ one(); two(); }
      if(rows==3){ one(); two(); three(); }
      if(rows==4){ one(); two(); three(); four(); }
      if(rows==5){ one(); two(); three(); four(); five(); }
      if(rows==6){ one(); two(); three(); four(); five(); six(); }
      if(rows==7){ one(); two(); three(); four(); five(); six(); seven(); }
      if(rows==8){ one(); two(); three(); four(); five(); six(); seven(); eight(); }
      if(rows==9){ one(); two(); three(); four(); five(); six(); seven(); eight(); nine(); }
    
   }


   module conerThing(){
      intersection(){
         // make corner bevel cube 1
         //OsBJ!:129
         translate([-40, -16, -.9]) // an_source mod
         rotate([0, 0, 45])
         cube([15, 15, 20.9]);

         // make corner bevel cube 1
         //OsBJ!:130
         translate([-40, -5, -.9]) // an_source mod
         rotate([0, 0, 0])
         cube([15, 15, 20.9]);
      }
   }


   module cornerHoles(){
      // corner post 1
      //OBJ!:111
      translate([-2.5, -2.5, -5]) // an_source mod
      rotate([0, 0, 0])
      cylinder(r = m3_rad, h = 30, center = false, $fn=160);
  
      // corner post 2
      //OBJ!:112
      translate([-2.5, 56.6, -5]) // an_source mod
      rotate([0, 0, 0])
      cylinder(r = m3_rad, h = 30, center = false, $fn=160);

      // corner post 3
      //OBJ!:113
      translate([72.1, 56.6, -5]) // an_source mod
      rotate([0, 0, 0])
      cylinder(r = m3_rad, h = 30, center = false, $fn=160);

      // corner post 4
      //OBJ!:114
      translate([72.1, -2.5, -5]) // an_source mod
      rotate([0, 0, 0])
      cylinder(r = m3_rad, h = 30, center = false, $fn=160);
   }


   module usb_and_power_holes(){
      //OBJ!:119
      translate([-13, 32.6, 6]) // an_source mod
      rotate([0, 0, 0])
      cube([15,13,12]);

      // make cube cutout for power jack
      //OBJ!:120
      translate([-13, 3.1, 7]) // an_source mod, y was 4.6
      rotate([0, 0, 0])
      cube([15,11,10]);
   }

   module makeCutOuts(){

      // put holes in corners first

      cornerHoles();

      // arduino mount bolt hole 1
      //OBJ!:115
      translate([66.65, 35.95, -5]) // an_source mod
      rotate([0, 0, 0])
      cylinder(r = m3_rad, h = 30, center = false, $fn=160);

      // arduino mount bolt hole 2
      //OBJ!:116
      translate([66.65, 8.02, -5]) // an_source mod
      rotate([0, 0, 0])
      cylinder(r = m3_rad, h = 30, center = false, $fn=160);

      // arduino mount bolt hole 3
      //OBJ!:117
      translate([14.58, 2.95, -5]) // an_source mod
      rotate([0, 0, 0])
      cylinder(r = m3_rad, h = 30, center = false, $fn=160);

      // arduino mount bolt hole 4
      //OBJ!:118
      translate([15.83, 51.18, -5]) // an_source mod
      rotate([0, 0, 0])
      cylinder(r = m3_rad, h = 30, center = false, $fn=160);

      usb_and_power_holes();

   } // end cutOuts()


   module makeBase3(){

      translate([-6, -6, -1]) // an_source mod
      rotate([0, 0, 0])
      cube([81.6, 66.2, 2.5]);
   }


   module makeBase2(){
        
      difference(){
           
         translate([-6, -6, -1]) // an_source mod
         rotate([0, 0, 0])
         cube([81.6, 66.2, 2.5]);     
       
         translate([-9, 39, -5])     
         makeHexGrid2(3,5,5);

      }
   }


   module makeBase(){

      makeBase2(); //this one has hex pattern

   } // end makeBase()
 

   module makeSides(){

      // make front wall
      //OBJ!:124
      translate([-6, -6, -.9]) // an_source mod
      rotate([0, 0, 0])
      cube([2.5, 66.2, 20.9]);

      // make back wall
      //OBJ!:125
      translate([73.1, -6, -.9]) // an_source mod
      rotate([0, 0, 0])
      cube([2.5, 66.2, 20.9]);

      // make side wall
      //OBJ!:126
      translate([-6, -6, -.9]) // an_source mod
      rotate([0, 0, 0])
      cube([81.6, 2.5, 20.9]);
  
      // make side wall
      //OBJ!:127
      translate([-6, 57.7, -.9]) // an_source mod
      rotate([0, 0, 0])
      cube([81.6, 2.5, 20.9]);

      // make cornor bevel thing 1
      //OBJ!:131
      translate([35, 0, 0])
      rotate([0, 0, 0])
      conerThing();
   
      // make cornor bevel thing 2
      //OBJ!:132
      translate([35.5, 53, 0]) // an_source mod
      rotate([0, 0, 180]) // an_source mod
      conerThing();
   
      // make cornor bevel thing 3
      //OBJ!:133
      translate([69.5, 35, 0]) // an_source mod
      rotate([0, 0, 90])
      conerThing();

      // make cornor bevel thing 4
      //OBJ!:134
      translate([1.1, 20, 0]) // an_source mod
      rotate([0, 0, -90])
      conerThing();

   } // end makeSides()



   module makeMountTab(){

      difference(){

         difference(){
            // make mounting tab
            //OBJ!:200
            translate([2, -26, -1]) // an_source mod
            rotate([0, 0, 0])
            cube([12, 18, 2.5]);

            // bolt hole in tab
            //OBJ!:201
            translate([8, -20, -5]) // an_source mod
            rotate([0, 0, 0])
            cylinder(r = Bolt_hole_od_10_24_inch_fit / 2 , h = 30, center = false, $fn=160);
         }
    
         // make rounded end
         translate([0, 16, 0]) // an_source mod
         rotate([0, 0, 0])
         difference(){
            difference(){
               translate([8, -36, -2]) // an_source mod
               rotate([0, 0, 0])
               cylinder(r = 9, h = 4, center = false, $fn=160);

               translate([8, -36, -3]) // an_source mod
               rotate([0, 0, 0])
               cylinder(r = 6, h = 6, center = false, $fn=160);
            }
            translate([-2, -36, -3]) // an_source mod
            rotate([0, 0, 0])
            cube([20,12,8]);
         }

      }

   } // end makeMountTab()



   module make4mountingTabs(){

      // make mounting tab 1
      //OBJ!:202
      translate([0, 4, 0]) // an_source mod
      rotate([0, 0, 0])
      makeMountTab();
   
      // make mounting tab 2
      //OBJ!:203
      translate([54, 4, 0]) // an_source mod
      rotate([0, 0, 0])
      makeMountTab();

      // make mounting tab 3
      //OBJ!:204
      translate([16, 50, 0]) // an_source mod
      rotate([0, 0, 180])
      makeMountTab();

      // make mounting tab 4
      //OBJ!:205
      translate([70, 50, 0]) // an_source mod
      rotate([0, 0, 180])
      makeMountTab();

   } // end make4mountingTabs()



   module makeSpacer(){

      difference(){
         translate([10, -40, 0]) // an_source mod
         rotate([0, 0, 0])
         cylinder(r = 6, h = 9, center = false, $fn=160);

         translate([10, -40, -10]) // an_source mod
         rotate([0, 0, 0])
         cylinder(r = Bolt_hole_od_10_24_inch_fit / 2 , h = 30, center = false, $fn=160);
      }

   } // end makeSpacer()

   
   module makeSpacer_x4(){

      adj = 17;

      makeSpacer();

      translate([adj, 0, 0])
      makeSpacer();
      
      translate([adj * 2, 0, 0])
      makeSpacer();

      translate([adj * 3, 0, 0])
      makeSpacer(); 

   }


   module makeSpacerWithFeet(){

      translate([0, -60, 0])
      makeSpacer();
      
      // add foot part
      translate([0, -60, -5])
      difference(){
         translate([10, -40, 0]) // an_source mod
         rotate([0, 0, 0])
         cylinder(r = 10, h = 5, center = false, $fn=160);

         translate([10, -40, -10]) // an_source mod
         rotate([0, 0, 0])
         cylinder(r = Bolt_hole_od_10_24_inch_fit / 2 , h = 30, center = false, $fn=160);

         // open up bigger hole for 10_24 bolt head
         translate([10, -40, -16]) // an_source mod
         rotate([0, 0, 0])
         cylinder(r = (Bolt_hole_od_10_24_inch_fit / 2)+4, h = 30, center = false, $fn=160);
      }      


      // make cylinder so we wont need support for printing
      // just will need to cut/drill out this shelf after
      // parts are printed
      translate([10, -40, 4.8]) // an_source mod
      translate([0, -60, -5])
      rotate([0, 0, 0])
      cylinder(r = 9, h = .9, center = false, $fn=160);

      
      difference(){
      translate([0, -60, 15.9])
      translate([10, -40, -16]) // an_source mod
         rotate([0, 0, 0])
         cylinder(r = 10, h = 3, center = false, $fn=160); 
   
      translate([0, -60, 14.9])
      translate([10, -40, -16]) 
         rotate([0, 0, 0])
         cylinder(r = Bolt_hole_od_10_24_inch_fit / 2 , h = 30, center = false, $fn=160); 
      }
      

   }

   module makeSpacerWithFeet_x4(){

      adj = 27;

      makeSpacerWithFeet();

      translate([adj, 0, 0])
      makeSpacerWithFeet();
      
      translate([adj * 2, 0, 0])
      makeSpacerWithFeet();

      translate([adj * 3, 0, 0])
      makeSpacerWithFeet(); 

   }

   module makeArduinoWash(){


      difference(){
         translate([10, -70, 0]) // an_source mod
         rotate([0, 0, 0])
         cylinder(r = 3, h = 2.3, center = false, $fn=160);

         translate([10, -70, -10]) // an_source mod
         rotate([0, 0, 0])
         cylinder(r = m3_rad, h = 30, center = false, $fn=160);
      }

   }


   module makeArduinoWash_x4(){

      adj = 17;

      makeArduinoWash();

      translate([adj, 0, 0])
      makeArduinoWash();
      
      translate([adj * 2, 0, 0])
      makeArduinoWash();

      translate([adj * 3, 0, 0])
      makeArduinoWash(); 

   }


   module makeMiddleSection(allow_usb_power_holes){

      // middle section...
      difference(){
         makeSides();
         makeCutOuts();
      } 
      
      // block out usb and jack hole on front side
      translate([-6, 0, 0]) // an_source mod
      rotate([0, 0, 0])
      cube([2.5, 50, 18]);
   
   }


   module makeMiddleSection2(allow_usb_power_holes){

      adj_x = 23;
      adj_z = 2;

      module mmm(){
         makeMiddleSection(allow_usb_power_holes);

         // make taller by 5 mm
         translate([0, 0, 5])
         makeMiddleSection(allow_usb_power_holes);
      }
     
      difference(){

         mmm();

         translate([-6 + (adj_x/2), -6 + -1.8, -.9 + 7]) // an_source mod
         rotate([0, 0, 0])
         cube([81.6 - adj_x, 2.5 + 2, 21.2 - adj_z]);

         // cutout for other side
         translate([-6 + (adj_x/2), -6 + -1.8 + 64, -.9 + 7]) // an_source mod
         rotate([0, 0, 0])
         cube([81.6 - adj_x, 2.5 + 2, 21.2 - adj_z]); 

         if(allow_usb_power_holes=="true"){
            usb_and_power_holes();
         }
      }
   }




   module makeMiddleSection3(allow_usb_power_holes){

      // this version has some small lips to hold in panels

      adj_x = 23;
      adj_z = 2;

      module mmm(){
         makeMiddleSection(allow_usb_power_holes);

         // make taller by 5 mm
         translate([0, 0, 5])
         makeMiddleSection(allow_usb_power_holes);
      }
     
      difference(){

         mmm();

         translate([-6 + (adj_x/2), -6 + -1.8, -.9 + 7]) // an_source mod
         rotate([0, 0, 0])
         cube([81.6 - adj_x, 2.5 + 2, 21.2 - adj_z]);

         // cutout for other side
         translate([-6 + (adj_x/2), -6 + -1.8 + 64, -.9 + 7]) // an_source mod
         rotate([0, 0, 0])
         cube([81.6 - adj_x, 2.5 + 2, 21.2 - adj_z]); 

         if(allow_usb_power_holes=="true"){
            usb_and_power_holes();
         }
      }


      // make lips


      //
      //y_tol = 0;  // add to y_tol to make wider gap for panel to fit in...
      y_tol = .15;  // 0 gap too small try .15 
                 


      // lip 1
      //
      translate([.8, -2.8 + y_tol, -.9])
      cube([5 ,1.5,20.9 - .5 + 5.5]);


      // lip 2
      //
      translate([.8 + 62.9, -2.8 + y_tol, -.9])
      cube([5 ,1.5,20.9 - .5 + 5.5]);
      

      // lip 3
      //
      translate([.8, -2.8 - y_tol + 58.3, -.9])
      cube([5 ,1.5,20.9 - .5 + 5.5]);


      // lip 4
      //
      translate([.8 + 62.9, -2.8 - y_tol + 58.3, -.9])
      cube([5 ,1.5,20.9 - .5 + 5.5]);



      /*
      //test cube 0, this one test thickness of lip on panel
      #translate([.8 + 5 + -2.5, -2.8 - .7 + -7.1, -.9 + 7])
      cube([1, .7, 1]);


      //test cube 1
      #translate([.8 + 5, -2.8 - .7, -.9])
      cube([1, .7, 1]);


      //test cube 1.1
      #translate([.8 + 5 + 57, -2.8 - .7, -.9])
      cube([1, .7, 1]);


      //test cube 2
      #translate([.8 + 5, -2.8 - .7 + 60.5, -.9])
      cube([1, .7, 1]);


      //test cube 2.2
      #translate([.8 + 5 + 57, -2.8 - .7 + 60.5, -.9])
      cube([1, .7, 1]);
      */

   }




module makeMiddleSection4(render_frame="true", render_panel="true", render_front_opening="true", render_back_opening="true"){

      // this version has space on front for panel

      adj_x = 23;
      adj_z = 2;

      y_offset = 21;

      // these are tol
      // less tol tighter fit
      // 
      y_offset_tol_panel = .6;  // .6
      z_offset_tol_panel = .6;  // .6   

      extend = 3; // bigger y and z  was 3, this is the lip

      allow_usb_power_holes = "false";

      module mmm(){
         makeMiddleSection(allow_usb_power_holes);

         // make taller by 5 mm
         translate([0, 0, 5])
         makeMiddleSection(allow_usb_power_holes);
      }
     
      
      if(render_frame=="true"){

         difference(){

            mmm();

            translate([-6 + (adj_x/2), -6 + -1.8, -.9 + 7]) // an_source mod
            rotate([0, 0, 0])
            cube([81.6 - adj_x, 2.5 + 2, 21.2 - adj_z]);

            // cutout for other side
            translate([-6 + (adj_x/2), -6 + -1.8 + 64, -.9 + 7]) // an_source mod
            rotate([0, 0, 0])
            cube([81.6 - adj_x, 2.5 + 2, 21.2 - adj_z]); 

            if(allow_usb_power_holes=="true"){
               usb_and_power_holes();
            }

            if(render_front_opening=="true"){
               // cutout for front
               //OBJ!:1124
               translate([-6 + -15 + 14, -6 + (y_offset/2), -.9 + 7]) // an_source mod
               rotate([0, 0, 0])
               cube([2.5 + 2, 66.2 - y_offset, 20.9 - 1]);
            }

            if(render_back_opening=="true"){ 
               // cutout for back
               //OBJ!:1125
               translate([-6 + -15 + 14 + 79, -6 + (y_offset/2), -.9 + 7]) // an_source mod
               rotate([0, 0, 0])
               cube([2.5 + 2, 66.2 - y_offset, 20.9 - 1]);
            }

         }

      }

      

      module ppp_panel(){   

         // panel for front back...
         //OBJ!:1126
         translate([-6 + -15 + 14 - 1 + 1, -6 + (y_offset/2) + (y_offset_tol_panel/2), -.9 + 7 + (z_offset_tol_panel/2)]) // an_source mod
         rotate([0, 0, 0])
         cube([2.5 + 2 - 1, 66.2 - y_offset - y_offset_tol_panel, 20.9 - 2 - z_offset_tol_panel]);

         // panel for front back...
         //OBJ!:1127
         translate([-6 + -15 + 14 - 1 - 2 + 1.3 + 1 + .5, -6 + (y_offset/2) + (y_offset_tol_panel/2) -(extend/2) , -.9 + 7 + (z_offset_tol_panel/2) - (extend/2)])  
         rotate([0, 0, 0])
         cube([2.5 + 2 - 2 - 1 - .5, 66.2 - y_offset - y_offset_tol_panel + extend, 20.9 - 2 - z_offset_tol_panel + extend]);

      }
 

      y_pos = -6 + (y_offset/2) + (y_offset_tol_panel/2);
      y_len = 66.2 - y_offset - y_offset_tol_panel;


      if(render_panel=="true"){

         difference(){

            ppp_panel();

            // smal m2 bolt hole to hold bottom of panel on

            // hole 1
            translate([-20, y_pos + (y_len/2), 7.8 + (z_offset_tol_panel - .6) ]) // an_source mod
            rotate([0, 90, 0])
            cylinder(r = 1.1, h = 30, center = false, $fn=160);
      
            // hole 2
            translate([-20, y_pos + (y_len/2), 7.8 + 20.9 - 2 - z_offset_tol_panel - 2.85 ]) // an_source mod
            rotate([0, 90, 0])
            cylinder(r = 1.1, h = 30, center = false, $fn=160);

         }

      }



      //
      //y_tol = 0;  // add to y_tol to make wider gap for panel to fit in...
      y_tol = .15;  // 0 gap too small try .15 


      if(render_frame=="true"){

         // make lips

         // lip 1
         //
         translate([.8, -2.8 + y_tol, -.9])
         cube([5 ,1.5,20.9 - .5 + 5.5]);

         // lip 2
         //
         translate([.8 + 62.9, -2.8 + y_tol, -.9])
         cube([5 ,1.5,20.9 - .5 + 5.5]);
      
         // lip 3
         //
         translate([.8, -2.8 - y_tol + 58.3, -.9])
         cube([5 ,1.5,20.9 - .5 + 5.5]);

         // lip 4
         //
         translate([.8 + 62.9, -2.8 - y_tol + 58.3, -.9])
         cube([5 ,1.5,20.9 - .5 + 5.5]);

         /*
         //test cube 0, this one test thickness of lip on panel
         #translate([.8 + 5 + -2.5, -2.8 - .7 + -7.1, -.9 + 7])
         cube([1, .7, 1]);

         //test cube 1
         #translate([.8 + 5, -2.8 - .7, -.9])
         cube([1, .7, 1]);

         //test cube 1.1
         #translate([.8 + 5 + 57, -2.8 - .7, -.9])
         cube([1, .7, 1]);

         //test cube 2
         #translate([.8 + 5, -2.8 - .7 + 60.5, -.9])
         cube([1, .7, 1]);

         //test cube 2.2
         #translate([.8 + 5 + 57, -2.8 - .7 + 60.5, -.9])
         cube([1, .7, 1]);
         */

      }

   }



   module makePanel(){

      adj_x = 23;
      adj_z = 2;

      // x_tol 
      // make smaller to fit pannel tighter
      x_tol = .5;
      
      z_tol = .3; // make smaller for pannel tighter in z plane up/down
   
      module p(){

         // hexgon cut out pannel realsize
         //OsBJ!:126
         translate([-6 + (adj_x/2) + (x_tol/2), -6 + -4, -.9 + 7]) // an_source mod
         rotate([0, 0, 0])
         cube([81.6 - adj_x - x_tol, 2.5, 20.9 - adj_z - z_tol]);

         // this makes a lip
         lip_extra = 3;
         //
         translate([-6 + (adj_x/2) + (x_tol/2) - (lip_extra/2), -6 + -4 - .6, -.9 + 7]) // an_source mod
         rotate([0, 0, 0])
         cube([81.6 - adj_x - x_tol + (lip_extra), .6, 20.9 - adj_z - z_tol]);
      }

      
      /*
      difference(){

         p();
         // make some small m2 bolt holes to hold pannel on

         translate([7.5, 0, 7 + ((20.9 - adj_z - z_tol)/2) - 1.1  ]) // an_source mod
         rotate([90, 0, 0])
         cylinder(r = 1.1, h = 30, center = false, $fn=160);

         translate([7.5 + 54.6, 0, 7 + ((20.9 - adj_z - z_tol)/2) - 1.1  ]) // an_source mod
         rotate([90, 0, 0])
         cylinder(r = 1.1, h = 30, center = false, $fn=160);
      }
      */

      p(); // dont need m2 holes anymore

   }


   // import("ArduBox2.stl");


   module makeTopBasePlate1(){

      difference(){
         makeBase();
         makeCutOuts();
      }
   }


   module makeFanHoles(){

      // main circle cutout
      translate([22.5, 22.5, 2])
      cylinder(r=19, center=false, h=15, $fn=160);

      // bolt hole 1
      translate([6.5, 6.5, 0])
      cylinder(r=2.3, center=false, h=15, $fn=160);

      // bolt hole 2
      translate([38.5, 6.5, 0])
      cylinder(r=2.3, center=false, h=15, $fn=160);

      // bolt hole 3
      translate([6.5, 38.5, 0])
      cylinder(r=2.3, center=false, h=15, $fn=160);
      
      // bolt hole 4
      translate([38.5, 38.5, 0])
      cylinder(r=2.3, center=false, h=15, $fn=160);

   }


   module makeTopBasePlate2(){
            
      difference(){

         makeBase3();
         //makeCutOuts();
         cornerHoles();
      
         translate([12.3, 4.65, -5])  // was 12
         makeFanHoles();

         /*
         //test thing to center makeFanHoles()
         translate([12, 5, -5])
         translate([-17.8, 5.4, 0]) 
         cube([21.7,2,15]);

         //test thing 2
         translate([12, 5, -5])
         translate([-17.8 + 59.4, 5.4, 0]) 
         cube([21.7,2,15]);

         //test thing 3
         translate([17.7, -5.6, -5])
         cube([2,14.5,15]);

         //test thing 4
         translate([17.7, 45.4, -5])
         cube([2,14.5,15]);
         */

      }

   }


   module makeStandardBaseBox(){

      // make sides
      difference(){
         makeSides();
         makeCutOuts();
      }   

      // make base
      difference(){
         makeBase();
         makeCutOuts();
      }
   
      // add mounting tabs to base
      make4mountingTabs();
   }




// just remove * from things you need to print...   1-11...




   // (1)
   // make bottom box
   makeStandardBaseBox();



   // (2)
   // make spacers
   //
   *makeSpacer_x4();



   // (3)
   // make spacers with feet to use box without mounting
   // just use 10/24 bolts upside down to make feet
   //
   *makeSpacerWithFeet_x4();




   // (4)
   // some little washers for the Arduino board if needed
   //
   *makeArduinoWash_x4();



   // (5)
   // standard middle section
   //
   //translate([0, 0, 30])
   //makeMiddleSection2(allow_usb_power_holes="false"); // no lip old design
   //
   // new middle section design
   //
   *translate([0, 0, 30])
   makeMiddleSection3(allow_usb_power_holes="false");  // this one has lip on cutout area



   // (6)   
   // make side panel with hex cutouts
   //
   // a good way to mount is with 2 m2, 6mm long bolts and just put 2 panels on each side back to back.
   //
   // note set radius_of_hexagons = 3; above
   //
   //rotate([90, 0, 0]) // to make stl
   *translate([0, 0, 30])
   difference(){
      makePanel();
   
      translate([5.5, 5, 19])     
      rotate([90, 0, 0]) 
      makeHexGrid2(4, 3, 3); 
   }
   



   // (7)
   // make a solid side pannel if needed
   //
   *translate([0, -20, 30])
   makePanel();
   


   // (8)
   // show a middle section with holes for usb and power plug
   //
   *translate([0, 0, 60])
   makeMiddleSection3(allow_usb_power_holes="true");




   // (9)
   // now another middle section with openings in front and rear
   // and solid panels for the opening. Note use m2 bolts 6mm long with
   // nuts washers etc to secure panels to opening.
   //
   // note: when you use this it must be used with the panel openings open toward the bottom.
   //       That way the two m2 bolts can be used to hold the panel/s in with the base piece
   //       or another middle section below this section.
   //      
   //
   *translate([0, 0, 60])
   makeMiddleSection4(render_frame="true", render_panel="false", render_front_opening="true", render_back_opening="false");
   //
   //
   // example of how to put useful holes in this panel also lay down for printing
   //
   //
   //translate([0, 0, 59.9])  // move to jive with makeMiddleSection4 above
   //rotate([0, 90, 0])
   //
   *difference(){
      rotate([0, 90, 0])
      rotate([0, 180, 0])
      makeMiddleSection4(render_frame="false", render_panel="true", render_front_opening="true", render_back_opening="false");
      //
      // cut out rectangle shape with rounded corners for wires etc...
      minkowski(){
         translate([-19.2 + .70, 9.5 + -.1, -10])
         cube([6, 35, 20]);
         cylinder(r=2, h = 2, $fn=160);
      }
   }
   //
   //
   // example of solid panel example layed down for printing...
   //
   *rotate([0, 90, 0])
   rotate([0, 180, 0])
   makeMiddleSection4(render_frame="false", render_panel="true", render_front_opening="true", render_back_opening="false");

    


   // (10)
   // another plate with hex holes and arduino mount holes
   //
   *translate([0, 0, 88])
   makeTopBasePlate1();



   // (11)
   // top plate with 40mm fan mount holes.
   //
   *translate([0, 0, 86.2])
   makeTopBasePlate2();









   module makeUnit(){
   
      makeStandardBaseBox();


      // make foot 1
      //OBJ!:550
      translate([-2, 84, -10.4]) // an_source mod
      rotate([0, 0, 0])
      makeSpacerWithFeet();

      // make foot 2
      //OBJ!:560
      translate([52, 84, -10.4]) // an_source mod
      rotate([0, 0, 0])
      makeSpacerWithFeet();

      // make foot 3
      //OBJ!:570
      translate([52, 170, -10.4]) // an_source mod
      rotate([0, 0, 0])
      makeSpacerWithFeet();

      // make foot 4
      //OBJ!:580
      translate([-2, 170, -10.4]) // an_source mod
      rotate([0, 0, 0])
      makeSpacerWithFeet();

   

      // make middle section
      //OBJ!:510
      translate([0, 0, 21.5]) // an_source mod
      rotate([0, 0, 0])
      makeMiddleSection3(allow_usb_power_holes="false");  // this one has lip on cutout area
   


      // make side panel 1
      //OBJ!:520
      translate([0, -13.4, 52.5]) // an_source mod
      rotate([-180, 0, 0]) // an_source mod
      difference(){
         makePanel();
         translate([5.5, 5, 19])     
         rotate([90, 0, 0]) 
         makeHexGrid2(4, 3, 3); 
      }



      // make side panel 2
      //OBJ!:530
      translate([0, 67.6, -38.3]) // an_source mod
      rotate([0, 0, 0]) // an_source mod
      translate([0, 0, 60])
      difference(){
         makePanel();
   
         translate([5.5, 5, 19])     
         rotate([90, 0, 0]) 
         makeHexGrid2(4, 3, 3); 
      }
      


      // top plate with 40mm fan mount holes.
      //OBJ!:500
      translate([0, 0, 48]) // an_source mod
      rotate([0, 0, 0]) // an_source mod
      makeTopBasePlate2();


   } // end makeUnit()



   *makeUnit();



}

DoIt();


